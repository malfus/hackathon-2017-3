/**
 * Created by jhenley on 5/11/2017.
 */
var localStream = null;
var video = document.querySelector('video');

$('button.preview').on('click', function () {
    navigator.mediaDevices.enumerateDevices().then(devices => {
        devices.filter(device => device.kind === 'videoinput').forEach(device => alert(device.deviceId + ' ' + device.label));
        let device = devices.filter(device => device.kind === 'videoinput').map(device => device.deviceId)[0];
        navigator.mediaDevices.getUserMedia({video: {
            mandatory: {
                maxWidth: 640,
                maxHeight: 360
            },
            deviceId: device
        }}).then(function (stream) {
            video.src = window.URL.createObjectURL(stream);
            localStream = stream;
            BarcodeReader.Init();
            BarcodeReader.StreamCallback = function(data) {
                if (data.length > 0) {
                    alert(data[0].Value);
                }
            };
            BarcodeReader.DecodeStream(video);
        })
    })
});

$('button.stop').one('click', function () {
    // stream.stop();
    BarcodeReader.StopStreamDecode();

    localStream.getTracks().forEach(function (track) {
        track.stop();
    })
})

$('button.scan').on('click', function() {
    var img = document.querySelector('img');
})