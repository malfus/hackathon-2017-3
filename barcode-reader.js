
var barcodeReader = {}
barcodeReader.shadowCanvas = document.createElement("canvas");
let DEFAULT_URL = '10.198.124.97:8080';

$(document).ready(function () {
    barcodeReader.player = $('.barcode-reader #player').get(0);
    barcodeReader.snapshotCanvas = $('.barcode-reader #snapshot').get(0);
    barcodeReader.captureButton = $('.barcode-reader #capture').get(0);
    barcodeReader.lastScan = '';
    barcodeReader.videoTracks;

	barcodeReader.chooseFile = $('.barcode-reader #chooseFile').get(0);
	barcodeReader.frame = $('.barcode-reader #frame').get(0);

	barcodeReader.chooseFile.addEventListener('change', function (e) {
		var file = e.target.files[0];
        targetHTML = $('.barcode-reader #upload .uploadResponse').get(0)
        barcodeReader.upload(file, targetHTML)
		barcodeReader.frame.src = URL.createObjectURL(file);
	});

    $('.barcode-reader .confirm-barcode').slideUp("slow", function () {});

    barcodeReader.captureButton.addEventListener('click', function () {
        var context = snapshot.getContext('2d');
        context.drawImage(barcodeReader.player, 0, 0, barcodeReader.snapshotCanvas.width, barcodeReader.snapshotCanvas.height);
        barcodeReader.shadowCanvas.getContext('2d').drawImage(barcodeReader.player, 0, 0, barcodeReader.player.videoWidth, barcodeReader.player.videoHeight);
    });

    $('.barcode-reader #stream-show').click(function() {
        $('.barcode-reader #stream').toggleClass('hidden');
        $('.barcode-reader #stream-show').toggleClass('hidden');
        $('.barcode-reader #stream-hide').toggleClass('hidden');
        $('.barcode-reader #upload').addClass('hidden')
        barcodeReader.play()
    });
    $('.barcode-reader #stream-hide').click(function() {
        $('.barcode-reader #stream').toggleClass('hidden');
        $('.barcode-reader #stream-show').toggleClass('hidden');
        $('.barcode-reader #stream-hide').toggleClass('hidden');
        $('.barcode-reader #upload').removeClass('hidden')
        barcodeReader.stop()
    });
    $('.barcode-reader #upload-button').click(function() {
        barcodeReader.shadowCanvas.toBlob(function(blob) {
            targetHTML = document.querySelector('.barcode-reader #stream .uploadResponse')
            barcodeReader.upload(blob, targetHTML)
        })
    });
})


barcodeReader.handleSuccess = function (stream) {
    // Attach the video stream to the video element and autoplay.
    barcodeReader.player.srcObject = stream;
    barcodeReader.videoTracks = stream.getVideoTracks();


    BarcodeReader.Init();
    BarcodeReader.StreamCallback = function (data) {
        if (data.length > 0) {
            var codes = data.map(item => item.Value);
            codes.forEach(code => {
                if (barcodeReader.lastScan !== code) {
                    barcodeReader.lastScan = code;
                    $.get(`http://localhost:8080/applianceDetails?model=${code}`, function(apiData) {
                        var product = JSON.parse(apiData).results[2].records.products[code][0];
                        var images = JSON.parse(apiData).results[2].records.products[code][3];
                        if (product) {
                            var text = `
										Brand: ${product.Brand[0]} <br>
										Color: ${product.Color[0]} <br>
										Description: ${product.Description[0]} <br>
										Dimensions: ${product.ProductDimensions[0]} <br>
										Release Date: ${product.Product_ADC_Availability_Date[0]} <br>
										Model Number: ${product.SKU[0]}
							`;
                            $('.confirm-model').html(text);
                            $('.barcode-reader .confirm-barcode').slideDown("slow", function () {});
                        }
                        if (images) {
                            var photoElement = Object.values(images.Images).find(value => value.Data_Group_ID === 'Product Photos_Un-installed/free-standing_Uninstalled Only');
                            var imageURL = `http://products.geappliances.com/MarketingObjectRetrieval/Dispatcher?RequestType=Image&Name=${photoElement.Name}&Variant=ViewLarger`;
                            var image = document.querySelector('.confirm-barcode img');
                            image.src = imageURL;
                        }
                    })
                }
            });
        }
    };
    BarcodeReader.DecodeStream(barcodeReader.player);
};



barcodeReader.stop = function () {
    barcodeReader.videoTracks.forEach(function (track) {
        track.stop()
    });
    BarcodeReader.StopStreamDecode();
}

barcodeReader.play = function () {

    navigator.mediaDevices.enumerateDevices().then(devices => {
        let device = devices.filter(device => device.kind === 'videoinput').map(device => device.deviceId)[1];
        navigator.mediaDevices.getUserMedia({
            video: {
                deviceId: device
            }
        })
            .then(barcodeReader.handleSuccess);
    });

}

barcodeReader.confirm = function () {
    $('.barcode-reader .confirm-barcode').slideUp("slow", function () {
        $('.barcode-reader .confirm-model').text('');
        document.querySelector('.barcode-reader .confirm-barcode img').src = '';
    });
}

barcodeReader.uploadCount = 0
barcodeReader.upload = function(file, targetHTML){
    var fd = new FormData();
    barcodeReader.uploadCount++
    fd.append('fname', barcodeReader.uploadCount + 'test.png');
    fd.append('file', file);
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/upload',
        data: fd,
        processData: false,
        contentType: false
    }).done(function (data) {
        console.log(data);
        var split = data.split('into ')
        console.log(split);
        if (split.length > 1) targetHTML.innerHTML = '<p>' + split[0] + 'to <a href=' +split[1] + '>' + split[1] + '</a></p>'
        else targetHTML.innerHTML = '<p>' + data + '</p>'
    });
}
